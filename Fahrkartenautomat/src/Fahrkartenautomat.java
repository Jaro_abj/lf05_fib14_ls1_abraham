﻿import java.util.Scanner;

class Fahrkartenautomat
{

    public static void main(String[] args)
    {
  
 
   	   // Fahrkarten Bestellung
       double zuZahlenderBetrag = fahrkartenbestellungErfassen(0);; 
       
       //Bezahlen und Rückgeldsenden
       rueckgeldSenden(fahrkartenBezahlen(zuZahlenderBetrag));
       // Fahrkarten ausgabe
       fahrkartenAusgeben();
  

    }
    
    public static double fahrkartenbestellungErfassen( double Summe) {
    	Scanner tastatur = new Scanner(System.in);
    	   boolean gültigeFahrkarte = false;
    	   boolean anotherCard = false;
    	   double zwischensumme  = 0; 
    	   double zuZahlenderBetrag = 0; 
    	   int anzahlDerTickets, Fahrkarte;
    	   String weitereKarte;
    	   double karte1 = 2.90;
    	   double karte2 =  8.60;
    	   double karte3 = 23.50;
  

    	   
    	   System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n"
    	   		+ "  Einzelfahrschein Regeltarif AB ["+karte1+" EUR] (1)\n"
    	   		+ "  Tageskarte Regeltarif AB ["+karte2+" EUR] (2)\n"
    	   		+ "  Kleingruppen-Tageskarte Regeltarif AB ["+karte3+" EUR] (3) \n  \n");
           
           while(!gültigeFahrkarte) {
        	   System.out.print(" Ihre Wahl: ");
        	   Fahrkarte = tastatur.nextInt();
  
         switch(Fahrkarte) {
         case 1: 
        	 zuZahlenderBetrag = 2.90;
        	 gültigeFahrkarte = true;
        	 break;
         case 2: 
        	 zuZahlenderBetrag = 8.60;
        	 gültigeFahrkarte = true;
        	 break;
         case 3: 
        	 zuZahlenderBetrag = 23.50;
        	 gültigeFahrkarte = true;
        	 break;
        	 
        	 default:
        		 System.out.println(">> Falsche Eingabe <<");
         }
           }
           
           System.out.print("Anzahl der Tickests: ");
           anzahlDerTickets = tastatur.nextInt();
           
        // Es dürfen nur zwischen 1 und 10 Tickets gekauft werden 
           boolean ticketanzahl = false;
    	   while(!ticketanzahl) {
           if(anzahlDerTickets > 10 || anzahlDerTickets < 0 ) {
        	   System.out.println("\n >> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
        	 
            	   System.out.print("Anzahl der Tickets: ");
            	   anzahlDerTickets = tastatur.nextInt();
            	   }else {
            		   ticketanzahl = true;
            	   }
        	   }

    	 zwischensumme =   anzahlDerTickets * zuZahlenderBetrag + Summe;
    	 System.out.printf("\n Zwischensumme: %.2f€%n", zwischensumme);
    	 
         while(!anotherCard) {
      	   System.out.println(" Willst du ein Weiteres Ticket Kaufen [Ja/Nein] ");
      	 weitereKarte = tastatur.next();
      	 System.out.print("\n");

       switch(weitereKarte) {
       case "ja": 
       case "Ja":
       case "y":
       case "Y":
       case "yes":
       case "j":
       case "J":
    	  zwischensumme =  fahrkartenbestellungErfassen(zwischensumme);
    	   anotherCard = true; 
      	 break;
       case "nein": 
       case "Nein":
       case "no":
       case "No":
       case "n":
       case "N":
    	   anotherCard = true; 
    	   return zwischensumme;
    	   
      	default:
      		 System.out.println(">> Falsche Eingabe <<");
       }
         }
    	 
   
    	 
    	 return zwischensumme;

          
  
 	   }
    
    public static double fahrkartenBezahlen(double Betrag) {
    	Scanner tastatur = new Scanner(System.in);
    	 double zuZahlenderBetrag = Betrag ;
    	 double eingezahlterGesamtbetrag;
         double eingeworfeneMünze;
    	 eingezahlterGesamtbetrag = 0.0;
    	 
         while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
         {
      	   System.out.printf("noch zu Zahlen: %.2f€%n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
      	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
      	   eingeworfeneMünze = tastatur.nextDouble();
             eingezahlterGesamtbetrag += eingeworfeneMünze;
         }

  

         // Rückgeldberechnung
         
         double wechselGeld = eingezahlterGesamtbetrag - zuZahlenderBetrag;
         return wechselGeld;
    	
    }
    
    public static void muenzeAusgeben(int betrag, String einheit) {
    	System.out.println(betrag + " " + einheit);
    }
    
    public static void rueckgeldSenden(double wechselGeld) {
    	double rückgabebetrag = wechselGeld;
    	  if(rückgabebetrag > 0.0)
          {
       	   System.out.println("\n Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
       	   System.out.println("wird in folgenden Münzen ausgezahlt:");

              while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
              {
            	  muenzeAusgeben(2, "Euro");
   	          rückgabebetrag -= 2.0;
              }
              while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
              {
            	  muenzeAusgeben(1, "Euro");
   	          rückgabebetrag -= 1.0;
              }
              while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
              {
            	  muenzeAusgeben(50, "Cent");
   	          rückgabebetrag -= 0.5;
              }
              while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
              {
            	  muenzeAusgeben(20, "Cent");
    	          rückgabebetrag -= 0.2;
              }
              while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
              {
            	  muenzeAusgeben(10, "Cent");
   	          rückgabebetrag -= 0.1;
              }
              while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
              
            	  muenzeAusgeben(5, "Cent");
    	          rückgabebetrag -= 0.05;
              }
          }
    
    public static void warte(int milliesekunden) {
        try {
   			Thread.sleep(milliesekunden);
   		} catch (InterruptedException e) {
   			// TODO Auto-generated catch block
   			e.printStackTrace();
   		}
    }
    
    
    public static void fahrkartenAusgeben() {
        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
     
        }
        
    	  System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                  "vor Fahrtantritt entwerten zu lassen!\n"+
                  "Wir wünschen Ihnen eine gute Fahrt.");
    }
}