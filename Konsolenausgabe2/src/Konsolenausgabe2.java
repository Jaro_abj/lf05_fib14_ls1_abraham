
public class Konsolenausgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		System.out.printf( "%10s\n", "**" ); 
		System.out.printf( "%6s", "*" );
		System.out.printf( "%7s\n", "*" );
		System.out.printf( "%6s", "*" );
		System.out.printf( "%7s\n", "*" );
		System.out.printf( "%10s\n", "**" );
		
		System.out.print("\n");
		 String s = "0!";
		System.out.printf("%-5s", s); 
		System.out.print("=");
		System.out.printf("%19s", ""); 
		System.out.print("=");
		System.out.printf("%4s\n", "1"); 
		
		String a = "1!";
		System.out.printf("%-5s", a); 
		System.out.print("=");
		System.out.printf("%-19s", " 1"); 
		System.out.print("=");
		System.out.printf("%4s\n", "1"); 
		
		String b = "2!";
		System.out.printf("%-5s", b); 
		System.out.print("=");
		System.out.printf("%-19s", " 1 * 2"); 
		System.out.print("=");
		System.out.printf("%4s\n", "2"); 
		
		String c = "3!";
		System.out.printf("%-5s", c); 
		System.out.print("=");
		System.out.printf("%-19s", " 1 * 2 * 3"); 
		System.out.print("=");
		System.out.printf("%4s\n", "6"); 
		
		String d = "4!";
		System.out.printf("%-5s", d); 
		System.out.print("=");
		System.out.printf("%-19s", " 1 * 2 * 3 * 4"); 
		System.out.print("=");
		System.out.printf("%4s\n", "24"); 
		
		String e = "5!";
		System.out.printf("%-5s", e); 
		System.out.print("=");
		System.out.printf("%-19s", " 1 * 2 * 3 * 4 * 5"); 
		System.out.print("=");
		System.out.printf("%4s\n", "120"); 
		
		System.out.print("\n");

		System.out.printf("%-2s|", "Fahrenheit");
		System.out.printf("%-5s", "Celsius");
		
	}
	

}
